var operation = document.getElementById('operations');
var resultdiv = document.getElementById('result-div');
function addChar(element) {
    var char = element.getAttribute('data-value');
    operation.innerText += char;
}
function cube() {
    var operationValue = Number(operation.innerText);
    operation.innerText = (operationValue * operationValue * operationValue).toString();
}
function clearScreen() {
    operation.innerText = "0";
}
function backspace() {
    var operationValue = operation.innerText;
    var operationValueLength = operationValue.length;
    var newOperationValue = operationValue.substring(0, operationValueLength - 1);
    operation.innerText = newOperationValue;
}
function calculate() {
    var operationValue = operation.innerText;
    operation.innerText = eval(operationValue);
}
// trigonometry function to calculate sine,cos,tan,cot,cosec,sec
function trigonometry(fun) {
    var operationValue = Number(operation.innerText);
    var result;
    switch (fun) {
        case 'sine':
            result = Math.sin(operationValue);
            break;
        case 'cos':
            result = Math.cos(operationValue);
            break;
        case 'tan':
            result = Math.tan(operationValue);
            break;
        case 'cot':
            result = 1 / Math.tan(operationValue);
            break;
        case 'cosec':
            result = 1 / Math.sin(operationValue);
            break;
        case 'sec':
            result = 1 / Math.cos(operationValue);
            break;
    }
    if (result === 'NaN') {
        operation.innerText = "Error";
    }
    else {
        operation.innerText = result;
    }
}
function func(fun) {
    var operationValue = Number(operation.innerText);
    var result;
    switch (fun) {
        case 'random':
            result = (Math.random() * 100);
            break;
        case 'ceil':
            result = Math.ceil(operationValue);
            break;
        case 'floor':
            result = Math.floor(operationValue);
            break;
        case 'round':
            result = Math.round(operationValue);
            break;
    }
    if (result === 'NaN') {
        operation.innerText = "Error";
    }
    else {
        operation.innerText = result;
    }
}
function square() {
    var operationValue = Number(operation.innerText);
    operation.innerText = (operationValue * operationValue).toString();
}
function absolute() {
    operation.innerText = (Math.abs(Number(operation.innerText))).toString();
}
function xp() {
    operation.innerText = (Math.exp(Number(operation.innerText))).toString();
}
function sqrt() {
    operation.innerText = (Math.sqrt(Number(operation.innerText))).toString();
}
function factorial() {
    var operationValue = operation.innerText;
    var result = 1;
    for (var i = Number(operationValue); i >= 1; i--) {
        result *= i;
    }
    operation.innerText = (result).toString();
}
function ten_pow() {
    operation.innerText = (Math.pow(10, Number(operation.innerText))).toString();
}
// log base 10 function
function log() {
    operation.innerText = (Math.log10(Number(operation.innerText))).toString();
}
//natural logarithm function
function ln() {
    operation.innerText = (Math.log(Number(operation.innerText))).toString();
}
// MC, MR, M+, M-, MS funtionality
var memoryArr = [];
var mStatus = 0;
function Clear() {
    memoryArr = [];
    if (mStatus == 1) {
        document.getElementById('memory-clear').style.opacity = (0.2).toString();
        document.getElementById('memory-recall').style.opacity = (0.2).toString();
    }
}
function Recall() {
    var sum = 0;
    for (var _i = 0, memoryArr_1 = memoryArr; _i < memoryArr_1.length; _i++) {
        var x = memoryArr_1[_i];
        sum += Number(x);
    }
    operation.innerText = sum.toString();
}
function Plus() {
    if (operation.innerText != "") {
        memoryArr.push(operation.innerText);
    }
    if (mStatus == 0 && operation.innerText != "") {
        document.getElementById('memory-clear').style.opacity = (1).toString();
        document.getElementById('memory-recall').style.opacity = (1).toString();
        mStatus = 1;
    }
    operation.innerText = "";
}
function Minus() {
    if (operation.innerText != "") {
        memoryArr.push('-' + operation.innerText);
    }
    if (mStatus == 0 && operation.innerText != "") {
        document.getElementById('memory-clear').style.opacity = (1).toString();
        document.getElementById('memory-recall').style.opacity = (1).toString();
        mStatus = 1;
    }
    operation.innerText = "";
}
function Save() {
    if (operation.innerText != "") {
        memoryArr.push(operation.innerText);
    }
    if (mStatus == 0 && operation.innerText != "") {
        document.getElementById('memory-clear').style.opacity = (1).toString();
        document.getElementById('memory-recall').style.opacity = (1).toString();
        mStatus = 1;
    }
    operation.innerText = "";
}
